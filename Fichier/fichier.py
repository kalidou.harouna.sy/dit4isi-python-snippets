import json
'''

    Existence de mode:

        Lecture : r
        Ecriture : w / a
        Lecture et Ecriture: r+ 
        binaire : b 

'''
try:
    fichier = open('../texte.txt','r+')
except(IOError):
    print("Votre fichier ne peut etre ouverts")
#else:
    #Recupre l'integralite du fichier
    #print(fichier.read())

    #Recupree ligne par ligne
    #print('1e ligne : ',fichier.readline())
    #print('2e ligne : ',fichier.readline())

    #Recupere l'ensemble des lignes sous la forme de liste
    #print('Reste des lignes : ',fichier.readlines())

    #ecrire dans un fichier texte - une chaine de caractere
    #fichier.write('Ceci est une chaine')


'''
Sauvegarde des objets de type compexe
'''

d={'nom': 'Ndiaye', 'prenom': 'Abdou'}


#d=[1,2,3]

#json.dump(d,fichier)

monobjet = json.load(fichier)

print(monobjet)