#Récupération des entrées au clavier avec la fonction input
#Input ne fait pas de retour à la ligne après avoir écrit le message. Il faut l'indiquer en ajoutant \n
nom = input("Veuillez saisir votre nom?\n")
prenom = input("Veuillez saisir votre prenom?\n")
age = input("Veuillez saisir votre age?\n")

#Utilisation de la méthode f-strings
print(f"Bonjour {prenom} {nom}. Vous avez {age} ans")
