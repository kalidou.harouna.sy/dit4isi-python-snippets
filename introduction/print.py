'''
Nous étudions la fonction print permettant de faire l'affichage sur la sortie par défaut.
Nous allons voir toutes les variantes possibles.
Ceci est un commentaire multiligne
'''

#Ceci est un commentaire uniligne
# \n dans une chaine permet un retour à la ligne (retour chariot)
# \t dans une chaine permet de faire une tabulation 
print("Bonjour \nJe m'appelle Demba Ndiaye \nJe suis etudiant en DIT4 ")
#la fonction print appelé avec une chaine vide affiche à l'écran un saut de ligne
print("") 
print(12)
#La fonction print peut prendre plusieurs paramètres séparés par des virgules
print("Bonsoir","DIT4", 12)
#La fonction print sans paramètre affiche à l'écran un saut de ligne
print()

#La fonction print dispose d'un paramètre facultatif est nommé <<end>> qui définit le caractère de fin de ligne
#Par défaut, le paramètre <<end>> est appelé de façon implicite avec la valeur \n si elle n'est pas mentionné
print("Bonjour",end="\n-::-\n")
print("Bonjour",end="\n#*#\n")

print("Bonjour", end="\t")
print("Bonsoir")

print("Bonjour\tBonsoir\tAurevoir")

#La fonction print dispose d'un paramètre facultatif est nommé <<sep>> qui définit le caractère de séparation entre les paramètres
#Par défaut, le paramètre <<sep>> est appelé de façon implicite avec la valeur " "(lire espace)
print("Bonsoir","DIT4",12)
print("Bonsoir","DIT4",12,end=" ** \n",sep=" # ")


#le caractère antislash permet d'écrire une chaine qui peut tenir sur plusieurs lignes(Suite)
print("Bonjour. Je suis à la première ligne \
Là c'est la suite de ma première ligne")

#Une façon d'afficher sur plusieurs lignes
print(
"""
+-----+-----+-----+
|  1  |  2  |  3  |
+-----+-----+-----+
|  4  |  5  |  6  |
+-----+-----+-----+
|  7  |  8  |  9  |
+-----+-----+-----+

-                 -
|  1     2     3  |

|  4     5     6  |

|  7     8     9  |
-                 _ 
"""
)
