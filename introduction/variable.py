#Définition variable

#variable de type entier
age=24

#variable de type flottant
taille=1.73

#variable de type Chaine de caractères
nom="Ndiaye"
prenom="Alioune"

#variable de type Booléen --> 2 valeurs possible (True ou False)
marie=True

#Affichage des varaibles avec print

#Méthode 1 : Paramètres multiples
print("===> Start method 1 <========")
print("Présentation de l'étudiant : ",nom,prenom)
print("* Age:",age,"ans")
print("* Taille:",taille,"mètres")
print("* Marié :",marie)
print()

#Méthode 2 : Substitution
print("===> Start method 2 <========")
print("Présentation de l'étudiant : %s %s"%(nom,prenom))
print("* Age: %s ans"%(age))
print("* Taille: %s mètres"%(taille))
print("* Marié : %s"%(marie))
print()

#Méthode 3 : Formatage
print("===> Start method 3 <========")
print("Présentation de l'étudiant : {} {}".format(nom,prenom))
print("* Age: {} ans".format(age))
print("* Taille: {} mètres".format(taille))
print("* Marié : {}".format(marie))
print()

#Méthode 4 : F-String
print("===> Start method 3 <========")
print(f"Présentation de l'étudiant : {nom} {prenom}")
print(f"* Age: {age} ans")
print(f"* Taille: {taille} mètres")
print(f"* Marié : {marie}")
print()



#Point d'attention : Addition chaine de caractère ==> concaténation

print("Présentation de l'étudiant : "+ nom +" "+ prenom)