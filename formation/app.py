from flask import Flask, request, redirect, abort, render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('frontend/accueil.html')


@app.route('/accueil')
def accueil():
    a=request.scheme
    b = request.remote_addr
    status_code = 200
    if b == '127.0.0.1':
        abort(500)
    if a == 'http':
        print("Yes")
        return redirect('https://seneweb.com')
    return a , status_code


@app.route('/profil/<login>/<identifiant>')
def profil(login, identifiant):
    return f'<h1>Bonjour {login} - id: {identifiant}</h1>'

