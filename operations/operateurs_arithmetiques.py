#toute donnée récupérée avec input est par défaut est de type chaine de caractères
'''
    Casting de type: conversion de type
        float --> entier
            exemple : int(12.5)
        entier --> float
            exemple: float(12)
        str --> float
            exemple: str(12.0)
        float --> str
            exemple : float("12.9")
        str --> int
            exemple : str(12)
        int --> str
            exemple : int("12")
'''
nombre1=input("Veuillez saisir le premier nombre: \t")
nombre2=input("Veuillez saisir le deuxième nombre: \t")

#Addition : Symbole +
resultat = int(nombre1) + int(nombre2)
print(f"{nombre1} + {nombre2} = {resultat}")

#Soustraction : Symbole -
resultat = int(nombre1) - int(nombre2)
print(f"{nombre1} - {nombre2} = {resultat}")

#Multiplication : Symbole *
resultat = int(nombre1) * int(nombre2)
print(f"{nombre1} x {nombre2} = {resultat}")

#Division entière : Symbole //
resultat = int(nombre1) // int(nombre2)
print(f"La division entière de {nombre1} par {nombre2} = {resultat}")

#Division réelle : Symbole /
'''
La fonction round permet d'arrondir d'un nombre
Syntaxe: round(nombre, precision)
NB: Si la précision n'est pas indiquée, alors puthon va faire un arrodi par défaut ou par excés 
'''

resultat = int(nombre1) / int(nombre2)
resultat = round(resultat,3)
print(f"La division réelle de {nombre1} par {nombre2} = {resultat}")

#Modulo (Reste de la division entière): Symbole %
resultat = int(nombre1) % int(nombre2)
print(f"Le reste de la division entière de {nombre1} par {nombre2} = {resultat}")

#Puissance: Symbole **
resultat = int(nombre1) ** int(nombre2)
print(f"{nombre1} à la puissances {nombre2} = {resultat}")

#NB: Elever un nombre à la puissance 1/2 revient à calculer sa racine carrée

resultat = int(nombre1)**0.5
print(f"La racine carrée de {nombre1} = {resultat}")

'''
True vaut 1
False vaut 0

True et False peuvent être utilisée avec les opérateurs arithmétiques
'''

#la fonction print évalue toute expression arithmétique avant de l'afficher
print(f"La racine carrée de {nombre1} = {int(nombre1)**0.5}")


#Priorité de opérateurs
'''
    (*) La multiplication et la division sont prioritaires devant l'addition et la soustraction

    (*) Les puissances sont les plus prioritaire

    (*) Si dans une expression arithmétique, nous avons que des multiplications et des divisions
    alors Python commence le calcul de la gauche vers la droite

    (*) Si dans une expression arithmétique, nous avons que des puissances
    alors Python commence le calcul de la gauche vers la droite

'''

resultat1 = 2 + 5 * 3 // 2 - 4
print("Le résultat de l'expression 2 + 5 * 3 // 2 - 4 est %s"%(resultat1))

resultat2 = 2 + 5 * 3 // 2 - 4**2
print("Le résultat de l'expression 2 + 5 * 3 // 2 - 4**2 est %s"%(resultat2))


#Si on veut forcer la priorité d'une opération, on utilise des parenthèses
resultat3 = (2 + 5) * 3 // (2 - 4)
print("Le résultat de l'expression (2 + 5) * 3 // (2 - 4) est %s"%(resultat3))


#Parmi ces opérateurs, deux sont compatibles avec les chaines de caractères

#Concacténation d'une chaine avec le symbole +
print("baba"+"Ndiaye")

#Duplication d'une chaine avec le symbole *. Attention l'un des opérandes doit être un entier
print("ciao!!"*3)