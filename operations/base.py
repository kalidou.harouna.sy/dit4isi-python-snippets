#Base 10 -> alphabet(0,1,2,3,4,5,6,7,8,9)
var_base10 = 9568
print("Ce nombre %s est défini dans la base 10"%(var_base10))

#Base 2 -> alphabet(0,1)
#Toujours précédé le nombre de 0b
var_base2 = 0b11010
print("Ce nombre 11010 défini dans la base 2 équivaut à %s"%(var_base2))

#Base 8 -> alphabet(0,1,2,3,4,5,6,7)
#Toujours précédé le nombre de 0o
var_base8 = 0o1234
print("Ce nombre 1234 défini dans la base 8 équivaut à %s"%(var_base8))

#Base 16 -> alphabet(0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F)
#Toujours précédé le nombre de 0x
var_base16 = 0xA12B
print("Ce nombre A12B défini dans la base 16 équivaut à %s"%(var_base16))


