'''
Opérateurs logiques : S'applique au Booleens

ET : (op1 and op2)
Le resultat sera égal à True si op1 et op2 valent True. False Sinon
+---------+---------+---------+
|         |  True   |  False  |
+---------+---------+---------+
|  False  |  False  |  False  |
+---------+---------+---------+
|   True  |   True  |   False |
+---------+---------+---------+

OU : (op1 or op2)
Le resultat sera égal à False si op1 et op2 valent False. True Sinon
+---------+---------+---------+
|         |  True   |  False  |
+---------+---------+---------+
|  False  |  True   |  False  |
+---------+---------+---------+
|   True  |  True   |   True  |
+---------+---------+---------+


NON : (not op1)

+---------+---------+---------+
|   op1   |  True   |  False  |
+---------+---------+---------+
|not op1  |  False  |  True   |
+---------+---------+---------+

'''

a=10
b=5
c=20

resultat = a<=b and b<=c
resultat1 = b<=a and a<=c
resultat2 = a<=b or b<=c

print(f"{a} est inférieur ou égal à {b} et {b} est inférieur ou égal à {c} : {resultat}")
print(f"{a} est inférieur ou égal à {b} ou {b} est inférieur ou égal à {c} : {resultat2}")

print(f"L'inverse - {a} est inférieur ou égal à {b} et {b} est inférieur ou égal à {c} : {not resultat}")
print(f"{b} est inférieur ou égal à {a} et {a} est inférieur ou égal à {c} : {resultat1}")

'''
Exemple: Année bissextile - une année est bissextile si elle est :
    * multiple de 4 et non multiple de 100
    * Ou multiple de 400
'''
annee = int(input("Merci de saisie une année à vérifier:\n"))
bissextile = (annee % 4 == 0 and annee % 100 != 0) or (annee % 400 == 0)
bissextile1 = (annee % 4 == 0 and not(annee % 100 == 0)) or (annee % 400 == 0)
print(f"L'année {annee} est bissextile : {bissextile}")
print(f"L'année {annee} est bissextile : {bissextile1}")