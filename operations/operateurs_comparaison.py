nombre1=int(input("Veuillez saisir le premier nombre: \t"))
nombre2=int(input("Veuillez saisir le deuxième nombre: \t"))

#Supérieur : Symbole >
resultat = nombre1 > nombre2
print(f"{nombre1} supérieur à {nombre2} : {resultat}")

#Supérieur ou égal : Symbole >=
resultat = nombre1 >= nombre2
print(f"{nombre1} supérieur ou égal à {nombre2} : {resultat}")

#Inférieur : Symbole <
resultat = nombre1 < nombre2
print(f"{nombre1} inférieur {nombre2} : {resultat}")

#Inférieur ou égal : Symbole <=
resultat = nombre1 <= nombre2
print(f"{nombre1} inférieur ou égal {nombre2} : {resultat}")

#Egalité : Symbole ==
resultat = nombre1 == nombre2
print(f"{nombre1} est égal à {nombre2} : {resultat}")


#Non-égalité (Différent): Symbole !=
resultat = nombre1 != nombre2
print(f"{nombre1} est différent de {nombre2} : {resultat}")


#Encadré un nombre par comparaison
a=10
b=5
c=20

resultat = a<=b<=c
resultat1 = b<=a<=c

print(f"{a} est inférieur ou égal à {b} qui est inférieur ou égal à {c} : {resultat}")
print(f"{b} est inférieur ou égal à {a} qui est inférieur ou égal à {c} : {resultat1}")

'''
NB: Il est possible d'utiser ces opérateurs avec des chaines de caractères. Dans ce cas,
une comparaison lexicographique sera effectuée.
'''