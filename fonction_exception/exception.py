from lib.standard import *

'''
    Pour gérer une exception, nous utilisons le block:

    try:
        instruction1
        intruction2
    except ClassesException:
        intruction3
    else:
        instruction4
    finall"y:
        instruction5
Dans le try, nous mettons les opérations succeptibles de produire des exceptions.

Dans le except, Nous capturons les exceptions et faisons le traitement. Nous 
pouvons avoir plusieurs blocs except.

La clase Exception permet de capturer tous les types d'exception.

Dans le else, nous mettons le code à executer que si aucune exception n'est détecté.

Dans le finally, nous mettons le code qui sera executé quelque soit alpha

'''
#Verifie si x est multiple de n
def multipleX(n,x):
    try:
        resultat = (n%x==0)
    except TypeError:
        print("Ooops !! Vous n'avez pas saisie un entier")
        resultat = False
    except ArithmeticError:
        print("Ooops !! La division par zero n'est pas autorisé")
        resultat = False
    else:
        print("Operation OK")
    finally:    
        return resultat 

n= saisie('ENTIER',"Saisir un entier n pour tester si n est multiple de x ")
x= saisie('ENTIER',"Saisir un entier x pour tester si n est multiple de x ")
print(f'{n} est multiple de {x} : {multipleX(n,x)}')


