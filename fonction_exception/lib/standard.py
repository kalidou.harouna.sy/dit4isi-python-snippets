def factoriel(n):
    resultat=1
    for compteur in range(1,n+1):
        resultat *= compteur
    return resultat 

#Définition de la fonction delta
def delta(b,c,a=0,d=0):
    return b**2 - 4*a*c

#Définition d'une fection factoriel récursive
def factRec(n):
    if n == 1 or n == 0:
        return 1
    else:
        return n * factRec(n-1)


#Définition fonction saisie valeur
'''
nature : CHAINE | ENTIER | FLOTTANT
'''
def saisie (nature, message):
    mavar = input(message+"\t")
    if nature == 'CHAINE':
        while(mavar.isalpha() == False):
            print("La valeur que vous avez saisie n'est pas une chaine. Merci de réessayer\n")
            mavar = input(message+"\t")
        return str(mavar)
    elif nature == 'ENTIER':
        while(mavar.isdigit() == False):
            print("La valeur que vous avez saisie n'est pas un entier. Merci de réessayer\n")
            mavar = input(message+"\t")
        return int(mavar)


