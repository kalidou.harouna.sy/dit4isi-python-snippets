from lib.standard import *
'''
    (*) La fonction c'est un bloc d'instructions qui prend en entrée 0 ou plusieurs et qui founit une sortie
    
    (*) Une procédure c'est une fonction qui ne renvoie pas de valeur

    (*) return est l'expression utilisée pour renvoyer une sortie dans une fonction

    (*) Les variables définies à l'intérieur d'une fonction sont locales à la fonction.

    (*) Les variables précédées de global permettent de garder la variable en dehors de la fonction

    (*) Les paramètres d'une fonction sont positionnels par défaut

    (*) Les paramètres nommés existent également et viennent toujours après les paramètres positionnels

    NB: La définition d'une fonction doit toujours précédé le premier appel de la fonction
'''
#Définition d'une fonction factoriel
n= saisie('ENTIER',"Saisir un entier n pour le calcul du factoriel")

resultat=1

f = factoriel(n)
print(f"calcul du factoriel de {n} = {f}")



'''
a=int(input("Donner le monome de degré 2\n"))
b=int(input("Donner le monome de degré 1\n"))
c=int(input("Donner la constante\n"))

mon_delta = delta(b,c,a)
print(f"Le delta de {a}x2 + {b}x + {c} = {mon_delta}")


#Définition d'une procédure factoriel
def factorielProc(n):
    resultat=1
    for compteur in range(1,n+1):
        resultat *= compteur
    print(f"Le factoriel de {n} est {resultat}")


#n=int(input("Donner une valeur\n"))
#Appel de procedure
factorielProc()
#Appel de fonction
print(f"Avant appel : {resultat}")
f = factoriel(n)
print(f"calcul du factoriel de {n} = {f}")
print(f"Après appel : {resultat}")
print(factRec(n))
'''
#