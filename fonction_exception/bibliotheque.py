'''
    (*) Fonctions Built-in viennent avec l'interpréteur de python par défaut et sont intégrés au noyau

    1e façon d'importer une bibliothèque: Importation totale de la librairie
        Instruction d'importation : import nomLibairie
        Utilisation: nomLibrairie.element

    2e façon d'importer une bibliothèque: plus adapté à ine importation partielle
        Importation d'importation:
            (*) Importation partielle
                from nomLibrairie import element1[, element2, ..., elementN] 
            (*) Importation totale
                from nomLibrairie import *
        Utilisation: element

    (*) "as" permet de définir des alias pour les éléments importés 
'''
#Utilisation 
#import math
from math import sqrt, pow, pi as complexe
r = int(input("Donner le rayon du cercler"))

surface = complexe * pow(r,2)

print(f"La surface du cercle de rayon {r} vaut {surface}")

