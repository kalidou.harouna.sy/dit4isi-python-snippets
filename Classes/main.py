from random import randint
from personnel import Etudiant
from classes import Ecole
import json


def saveData(data):
    fichier = open('../texte.txt','w')
    json.dump(data,fichier)
    fichier.close()


def loadData():
    try:
        fichier = open('../texte.txt','r')
    except FileNotFoundError:
        print("Le fichier sera créé au prochain ajout d'un élément")
        data=dict()
    else:
        data=json.load(fichier)
        fichier.close()
    return data

#programme d'appel de notre classe

mesecoles=loadData()

nom = input("Donner le nom de l'école? \t")

adresse = input(f"Donner l'adresse de l'ecole {nom}? \t")
anneeCreation = input(f"Donner l'année de création de l'école {nom}? \t")

monecole = Ecole(nom, adresse, anneeCreation)
monecole.whois()

print(monecole.__dict__)
mesecoles[monecole.code] = json.dumps(monecole.__dict__)
saveData(mesecoles)

nom = input("Donner le nom de la filière? \t")

mafiliere = Filiere(nom)

ecole = 'Bonjour'
etudiant = Etudiant('Ndiaye','Saliou')
mafiliere.affecterFiliere(etudiant)

mafiliere.showFiliere()