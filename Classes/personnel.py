from random import randint
'''
    Assurer la gestion des RHs
        - professseurs
        - etudiants
        - surveillants

    Notion Héritage en python
'''

class Personne():

    nom=None
    prenom=None
    dateNais=None
    profil=None
    code=None

    def __init__(self,e_nom, e_prenom):
        self.nom = e_nom
        self.prenom = e_prenom


class Etudiant(Personne):
    formation=None
    niveau=None

    def __init__(self,str_nom,str_prenom):
        super().__init__(str_nom,str_prenom)
        self.profil="Etudiant"
        self.code = 'ERH-'+str(randint(12000,99999))


    def affecterFormation(self, formation):
        self.formation = formation
    
    def leveldefine(self, niveau):
        self.niveau = niveau

    def showEtudiant(self):
        print(f"{self.nom} - {self.prenom} - {self.profil} - {self.formation} - {self.niveau}")

'''
nom = input("Donner le nom de l'étudiant?\t")
prenom = input("Donner le nom de l'étudiant?\t")

etudiant = Etudiant(nom,prenom)

formation = input(f"indiquer la formation de {etudiant.nom} {etudiant.prenom}?\t")
niveau = input(f"indiquer la formation de {etudiant.nom} {etudiant.prenom}?\t")

etudiant.affecterFormation(formation)
etudiant.leveldefine(niveau)

etudiant.showEtudiant()
'''