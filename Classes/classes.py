from random import randint
from personnel import Etudiant
'''
    Gestion des classes

    **** Déclaration *****
    class nom_Classe(classe_parente):
        corps de la classe (attribut, méthodes, constructeur)
    
    **** Appel à la fonction *****

    nomObjet = nomClasse(paramètres du constructeur)


'''


class Ecole():

    #Déclaration d'attribut
    nom=None
    adresse=None
    anneeCreation=None
    code=None

    #Constructeur
    def __init__(self, nom, adresse,anneeCreation):
        self.nom = nom
        code = randint(6000,10000)
        self.code = "ECO-"+str(code)
        self.adresse = adresse
        self.anneeCreation = anneeCreation
    
    def whois(self):
        print(f"Codification: {self.code} - Ecole {self.nom} - {self.anneeCreation} - {self.adresse}")

    def __str__(self):
        return self.nom


class Filiere():
    nom=None
    code=None
    ecoles=dict()

    def __init__(self,nom):
        self.nom = nom
        code = randint(6000,10000)
        self.code = 'FLE-'+str(code)
    
    def affecterFiliere(self,ecole):
        if isinstance(ecole,Ecole):
            self.ecoles[ecole.code] = ecole
        else:
            print("Merci d'ajouter un objet de type Ecole")
    
    def showFiliere(self):
        print(self.nom)
        print(self.code)
        print(self.ecoles)


