chaine=input("Indiquer une chaine : ")
palindrome = chaine == chaine[::-1]
print(f"'{chaine}' est un palindrome : {palindrome}")