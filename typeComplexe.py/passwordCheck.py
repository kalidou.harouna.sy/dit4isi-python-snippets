'''
    Enoncé : Ecrire un programme qui permet de vérifier la qualité d'un mot de passe
        - Doit contenir au moins 8 caractères et au plus 15 caractères
        - doit contenir une majuscule
        - doit contenir un chiffres
        - doit contenir un ou plusieurs lettres
        - doit contenir un caractère spécial(#&!-_$*)
'''

def passwordCheck(password):
    longueurOK=False
    majOK=False
    digitOK=False
    lettreOK=False
    specialOK=False
    varSpecial="#&!-_$*"
    while True:
        longueur = len(password)
        if 8<=longueur<=15:
            for i in password:
                if i.isupper():
                    majOK=True
                if i.isdigit():
                    digitOK=True
                if i.isalpha():
                    lettreOK=True
                if i in varSpecial:
                    specialOK=True
            if(majOK and digitOK and lettreOK and specialOK):
                print("Bingo !!! your password is very strong !!!")
                break
            else:
                print("Your password doesn't match the required policies!!! ")
                password = input("Please enter your password again? \n")
        else:
            print("Your password must contain at less 8 characters and at more 15 characters")
            password = input("Please enter your password again? \n")

password = input("Please enter your password? \n")
passwordCheck(password)