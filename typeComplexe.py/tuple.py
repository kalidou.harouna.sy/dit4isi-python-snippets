'''
    Les tuples:
        Sont de type complexe
        Peuvent un ou plusieurs éléments de type simple et/ou complexe
        Sont ordonnés
        Les indices sont numériques (entier)
        Sont non modifiables
        Syntaxe : (element1, element2,...,elementN)
    
    Chaque élément d'un tuple est repéré par un indice
    Les indices comment à partir de zéro
    Accès aux éléments : nomTuple[indice]

    Sous-tuple: nomTuple[start:stop:step]

'''

montuple=('Bonjour',1234,56.8,True)

montuple2=('Aurevoir','Bonjour')

#Concacténation d'un tuple
montuple3=montuple+montuple2

print(montuple)
print(montuple2)
print(montuple3)

print(type(montuple))

print(montuple[1])

#Compter le nombre d'occurrence d'un élément dans un tuple
print(montuple.count("Bonjour"))



#Parcourir un tuple

#Méthode1

for i in range(len(montuple)):
    print(montuple[i], end='-*-')

print()
#Méthode2
for i in montuple:
    print(i, end='-$-')

print()


#un sous-tuple
print(montuple[0:1])