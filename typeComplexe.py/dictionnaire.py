'''
    Les dictionnaires:
        Sont de type complexe
        Peuvent un ou plusieurs éléments de type simple et/ou complexe
        ne sont pas ordonnés
        Sont modifiables
        Les indices sont alpha-numériques (lettre ou entier)
        Syntaxe : {key1 : value1, key2: value,...,KeyN : valueN}
        Création d'une liste vide : {} ou dict()
    
    Chaque élément d'un dictionnaire est repéré par une clé
    Accès aux éléments : nomDictionnaire[key]

'''

#Création d'un dictionnaire vide
D1 = dict()
D2 = {}

#Création d'un dictionnaire non vide

D3 = {'nom': 'Gueye', 'prenom': 'Alioune', 'age': 17, 'marie': True }

classe = {'ISI54RT': D3}

print(D3)

#Ajouter un élément dans un dictionnaire
D1['python']=12
D1['math']=13

print(D3['prenom'])

#get : permet d'accéder à un élément

print(D1.get('Java','La note de java n\'existe pas'))

#Récupération des éléments d'un dictionnaire
print(D3.items())

#Récupération uniquement des clés

print(D3.keys())

#Récupération uniquement des valeurs

print(D3.values())


#Parcourir à partir des clés
print("**** Parcourir avec les clés")
for i in D3.keys():
    print(f"{i} est la clé associée à la valeur {D3[i]}")

print("**** Parcourir avec les valeurs")
for i in D3.values():
    print(f"{i} appartient au   dictionnaire")


print("**** Parcourir avec les items")
for (i,j) in D3.items():
    print(f"{i} est la clé associée à la valeur {D3[i]}")
