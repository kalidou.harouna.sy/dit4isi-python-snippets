'''
    Pour définir une chaine, nous pouvons utiliser les quotes simples(')
    ou les quotes doubles(")

    \ : permet d'échapper un caractère
    len : permet d'afficher la longueur d'une chaine

    Pour plus de fonctions : 
        - taper python sur la console
        - taper help(str)

    une sous chaine est définie de la sorte : chaine[start:stop:step]
        - créera une sous chaine allant de l'indice start à l'indice stop-1 par pas de step
'''

chaine = "ceci est une chaine"
long1 = len(chaine)
print(f"'{chaine}' est de longueur : {long1}")

'''
chaine2 = 'ceci est l\'ajout d\'une deuxième chaine'
long2 = len(chaine2)
print(f"'{chaine2}' est de longueur : {long2}")

#Exemple - solution 1 : Parcourir une chaine de caractères
chaine = input("Entrer la chaine à parcourir \n")
longueur_chaine = len(chaine)
for compteur in range (longueur_chaine):
    print(chaine[compteur], end=' - * -')

print()

#Exemple - solution 1 : Parcourir une chaine de caractères
chaine = input("Entrer la chaine à parcourir \n")
for compteur in chaine:
    print(compteur)
print()

#Fonctions applicables aux chaines de caractères

print("Chaine avant transformation : "+chaine)
#Capitalize : Met en majuscule la première lettre et le reste en miniscule
print(chaine.capitalize())

#upper : Met en majuscule toutes les lettres en miniscule
print(chaine.upper())


#lower : Met en miniscule toutes les lettres en miniscule
print(chaine.lower())

#count : Compte le nombre d'occurrence d'une sous chaine

print(chaine.count('ne'))
#Cherche la sous-chaine "ne" sur la première moitié de la chaine
print(chaine.count('ne',0,len(chaine)//2))

#endswith : verifie si la chaine se termine par
n = input("Donne rla distance en précisant l'unité")

if(n.endswith('km')):
    print("La distance est exprimé en kilomètres")
elif(n.endswith('mm')):
    print("La distance {} est exprimé en millimètres".format(n))
else:
    print("La distance n'est pas indiqué suivant les unités attendues")
'''
#Les sous-chaines

print("La chaine de l'indice 2 à l'indice 8 : "+chaine[2:9])
print("La chaine de l'indice 0 à l'indice 7 : "+chaine[:8])
print("La chaine de l'indice 5 jusqu'à la fin : "+chaine[6:])
print("La chaine en entier: "+chaine[:])
print("La chaine de l'indice 0 à l'indice 7 : "+chaine[2:-1:3])
print("La chaine de l'indice 0 à l'indice 7 : "+chaine[-1:0:-1])

#Inversion d'une chaine
print("La chaine en entier: "+chaine[::-1])