'''
    Les listes:
        Sont de type complexe
        Peuvent un ou plusieurs éléments de type simple et/ou complexe
        Sont ordonnés
        Sont modifiables
        Les indices sont numériques (entier)
        Syntaxe : [element1, element2,...,elementN]
        Création d'une liste vide : [] ou list()
    
    Chaque élément d'une liste est repéré par un indice
    Les indices comment à partir de zéro
    Accès aux éléments : nomListe[indice]

    Sous-liste: nomTuple[start:stop:step]

'''

#Création d'une liste vide

l1 = []
l2 = list()

dateJour=(18,'Août',2020,[0,6,12,18,24])

#Création d'une liste non vide
l3 = ['Bonsoir',True, dateJour]

#append: ajoute un élément à la fin de la liste
l1.append(12)
l1.append("Bonjour")

#concacténation d'une liste
l2 = l1 + l3

#extension d'une liste
l1.extend(l2)

#index: retourne l'indice d'un élément
print(l2.index('Bonjour'))

#count: renvoie le nombre d'occurence d'un élément
print(l2.count("Bonjour"))

#Accès au éléments
print(l2)
l2.insert(1,'intrus')
print(l2)



#Suppression d'un élément à partir de la valeur
try:
    l2.remove('Bonjour')
    print(l2)
except ValueError:
    print("Attention cette valeur n'est pas dans la liste")

#Suppression d'un élément à partir de son indice
try:
    a = l2.pop(1)
    print(f"la valeur supprimée est  : {a}")
    print(l2)
except IndexError:
    print("Attention cette valeur n'est pas dans la liste")



L=[1,8,4,7]
print("_________")
print(L)

#reverse: permet d'inverser une liste
L.reverse()
print(L)

#sort: permet de trier une liste
#Tri dans l'ordre croissant
L.sort()
print(L)
#Tri dans l'ordre décroissant
L.sort(reverse=True)
print(L)


#Sous liste

print(L[0:4:2])
print(L[0:4])
print(L[::-1])


#Parcours d'une liste

#Méthode1

for i in range(len(L)):
    print(L[i], end='-*-')

print()
#Méthode2
for i in L:
    print(i, end='-$-')

print()


#Copy d'une liste

L2 = L # L2 et L pointent sur la même liste. toute modification de L2 impacte L

L2.pop(0)
print(L2)
print(L)

print('*****')
L2 = L.copy() #crée une copie non lié - indépendante
L2.pop(0)
print(L2)
print(L)


print()