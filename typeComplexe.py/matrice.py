'''
    Les matrices


    M(i,j)= Matrice contenant i lignes et j colonnes

    Si i=j, alors nous avons une matrice carrèe
    

    Exemple de matrice carrée avec i,j=3
    
        1   2   3
        4   5   6
        7   8   9

        [ [1 , 2 , 3 ], [4 , 5 , 6 ], [7 , 8 , 9 ] ]

        [ [1 , 4 , 7 ], [2 , 5 , 8 ], [3 , 6 , 9 ] ]

    Chaque élément de la matrice est repéré par un couple i,j définissant sa position (aij)

        Exemple : Dans la matrice précédente

                a11 = 1   a22 = 5

                a31 = 7

    Observation
        Transformation par ligne

            L1 = [1,   2,   3]
            L2 = [4,   5,   6]
            L3 = [7,   8,   9]

            A = [ L1, L2, L3 ] = [[1,   2,   3], [4,   5,   6], [7,   8,   9]]

            Matrice en python: une liste de listes

            a11
                Ligne(Position par rapport à la liste A) : 0
                Colonne (Position par rapport à la liste A): 0

'''


#Fonction pour créer une matrice

def createMatrix():
    n=int(input("Donner le nombre de lignes de votre matrice? \t"))
    m=int(input("Donner le nombre de colonnes de votre matrice? \t"))

    #création de la matrice
    A = []

    for i in range(n):
        L=[]
        for j in range(m):
            value = int(input(f' Merci d\'indiquer la valeur de a{i+1}{j+1}\n'))
            L.append(value)
        A.append(L)

    return A

#Fonction affichage matrice
def cssMatrix(A):
    n=len(A)
    m=len(A[0])
    for i in range(n):
        for j in range(m):
            print(A[i][j],end='\t')


#Appel de la fonction - création de matrice

M=createMatrix()

cssMatrix(M)

def rotateMatrix(A):
    n=len(A)
    m=len(A[0])
    B=[]
    for j in range(n):
        LB=[]
        for i in range(n-1,-1,-1):
            LB.append(A[i][j])
        B.append(LB)

    return B

print("Affichage rotation matrice: ")
cssMatrix(rotateMatrix(M))


def rotateInverseMatrix(A):
    n=len(A)
    m=len(A[0])
    B=[]
    for j in range(n-1,-1,-1):
        LB=[]
        for i in range(n):
            LB.append(A[i][j])
        B.append(LB)

    return B

print("Affichage rotation matrice: ")

cssMatrix(rotateInverseMatrix(M))