n = int(input("Indiquer la valeur de n? \t"))

'''
#Solution 1 - Question 1
reste = n%5
if reste == 0:
    N = n
else:
    if 1<=reste<3:
        N = n - reste
    else:
        N = n + (5-reste)
'''
#Solution 2 - Question 1 
quotient = round(n/5)
print(f"Quotient : {quotient}")
N = quotient*5


#Solution - Question 2
h = input("Indiquer l'heure'? \t")
h_int = int(h)
m = input("Indiquer les minutes? \t")
m_int=int(m)
if not(0<=h_int<24 and 0<=m_int<60):
    print("L'heure saisie n'est pas correcte")
else:
    M = round(m_int/5)*5
    H = h_int
    if M >= 60:
        M = 0
        H = h_int + 1
        if h >=24:
            h_int = 0
    print(f"{h}h {m}m -> [{H}, {M}]")

print(f"La variable {N} est le multiple de 5 le plus proche de {n}")
