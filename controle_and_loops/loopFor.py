'''
for  --> bloc d'instructions 


for condition:
    intruction1
    instruction2
print("Fin de la boucle for")

La condition est principalement bâtie avec la fonction range:
(*) Syntaxe : range([start], stop, [step]) avec start, stop et step des entiers
(*) par défaut le step vaut 1 s'il n'est pas renseigné
(*) Details : générer une séquence ordonnée de nombre entiers allant de start à stop-1 par pas de step
'''
n = int(input("Entrer le nombre dont vous voulez calculez la table de multiplication par 10"))
'''
#Exemple 1 : Utilisation de for avec range(start, stop)
for compteur in range(1,11):
    resultat = n * compteur
    print(f"{n} X {compteur} = {resultat}")

#Exemple 2 : Utilisation de for avec range(start, stop, step)
#NB : Step ne peut pas être utilisés sans que start et stop soient défins
for compteur in range(1,11,1):
    resultat = n * compteur
    print(f"{n} X {compteur} = {resultat}")

#Exemple 3 : Utilisation de for avec range(stop)
#NB : Le compteur va démarrer à partir de 0
for compteur in range(11):
    resultat = n * compteur
    print(f"{n} X {compteur} = {resultat}")


#Exemple 3 : Utilisation de for --> sens décroissant : range(start, stop, step)
for compteur in range(10,0,-1):
    resultat = n * compteur
    print(f"{n} X {compteur} = {resultat}")



#Exemple 4 :  calcul factoriel

n = int(input("Entrer le nombre dont vous voulez calculez le factoriel \n"))
resultat=1
for compteur in range(1,n+1):
    resultat *= compteur

print(f"{n}! = {resultat}")


#Exemple 5 : Calcul du factoriel des nombres pairs compris entre 1 et n
# Highlight : Imbrication boucle while / Instruction continue
for compteur in range (1,n+1):
    resultat=1
    if(compteur%2!=0):
        #Continue permet d'arrêter l'itération en cours et de passer à l'itéartion suivante
        continue
    for compteur1 in range(1,compteur+1):
        resultat *= compteur1
    print(f"{compteur}! = {resultat}")
    compteur+=1


#Exemple - solution 1 : Parcourir une chaine de caractères
chaine = input("Entrer la chaine à parcourir \n")
longueur_chaine = len(chaine)
for compteur in range (longueur_chaine):
    print(chaine[compteur], end=' - * -')
print()
'''

#Exemple - solution 1 : Parcourir une chaine de caractères
chaine = input("Entrer la chaine à parcourir \n")
for compteur in chaine:
    print(compteur)
print()
