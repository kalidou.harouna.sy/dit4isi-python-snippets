'''
Bloc d'instructions.

    Keyword [conditions]:
        Instrunction 1
        Instruction 2
        ...
        Instruction n
    print("Bonjour")

    Exemple de Keyword: if, [elif], [else]
'''

#Exemple de bloc if

age = int(input("Indiquer votre âge? \t"))
nationalite = input("Indiquer votre nationalité? \t")
'''
if age >= 35:
    print("Vous êtes eligible au poste de président de la République")
print("fin de la structure de contrôle")

#Exemple de bloc if - else
if age >= 35:
    print("Vous êtes éligible au poste de président de la République")
else:
    print("Vous n'êtes pas éligible au poste de président de la République")

# Exemple de bloc if-else imbriqué
if age >= 35:
    #La fonction lower permet de convertir une chaine de caratères en miniscules
    if nationalite.lower() == "senegalais" or nationalite.lower() == "senegalaise":
        print("Vous êtes éligible au poste de président de la République")
    else:
        print("Désolé. Votre nationalité ne vous permet pas d'être président(e)")
else:
    print("Vous n'êtes pas éligible au poste de président de la République")
'''
#Utilisation de if-elif-else

if age >= 35 and (nationalite.lower() == "senegalais" or nationalite.lower() == "senegalaise"):
    print("Vous êtes éligible au poste de président de la République")
elif age >= 35 and (nationalite.lower() != "senegalais" or nationalite.lower() != "senegalaise"):
    print("Désolé. Votre nationalité ne vous permet pas d'être président(e)")
else:
    print("Vous n'êtes pas éligible au poste de président de la République")


#Vous pouvez avoir plusieurs bloc elif
#Vous pouvez ne pas avoir de bloc else ou elif (Seul if est obligatoire)