
'''
while --> bloc d'instructions dont l'instuction spéciale permet changer les valeurs
qui interviennent dans la condition afin de produire un état de sortie de la boucle


while condition:
    intruction1
    instruction2
    instruction_speciale
print("Fin de while)
'''



#Exemple de table de multiplication
'''
n = int(input("Entrer le nombre dont vous voulez calculez la table de multiplication par 10"))
compteur = 1
while compteur <= 10:
    resultat = n * compteur
    print(f"{n} X {compteur} = {resultat}")
    compteur +=1



#Exemple calcul factoriel

n = int(input("Entrer le nombre dont vous voulez calculez le factoriel \n"))
compteur = 1
resultat=1
while compteur <= n:
    resultat *= compteur
    compteur +=1

print(f"{n}! = {resultat}")


#Exemple 1 : Calcul du factoriel des nombres pairs compris entre 1 et n
# Highlight : Imbrication boucle while / Instruction continue
n = int(input("Entrer le nombre dont vous voulez calculez le factoriel \n"))
compteur = 1
while compteur <= n:
    resultat=1
    compteur1=1
    if(compteur%2!=0):
        #Continue permet d'arrêter l'itération en cours et de passer à l'itéartion suivante
        compteur+=1
        continue
    while compteur1 <=compteur:
        resultat *= compteur1
        compteur1 +=1
    print(f"{compteur}! = {resultat}")
    compteur+=1


#Exemple 2 : Calcul du factoriel des nombres pairs compris entre 1 et n
# Highlight : Instruction continue
n = int(input("Entrer le nombre dont vous voulez calculez le factoriel \n"))
compteur = 1
while compteur <= n:
    resultat=1
    resultat *= compteur
    if(compteur%2!=0):
        #Continue permet d'arrêter l'itération en cours et de passer à l'itéartion suivante
        compteur+=1
        continue
    print(f"{compteur}! = {resultat}")
    compteur+=1

'''

'''
           ---------------
    indice |0|1|2|3|4|5|6|
           ---------------
    valeur |B|O|N|J|O|U|R|
           ---------------

    (*)Pour accéder aux caractères d'une chaine : nomVariable[indice]
    (*)Les indices commencent à 0 et s'arrêtent à (longueur_chaine -1) dans le sens où l'on
    parcours la chaine de la gauche vers la droite
    (*)Les indices commencent à -1 et s'arrêtent à -longueur_chaine dans le sens où l'on
    parcours la chaine de la droite vers la gauche
    (*) la fonction len(objet) permet de déterminer la longeur de l'objet

#Exemple - solution 1 : Parcourir une chaine de caractères
chaine = input("Entrer la chaine à parcourir \n")
compteur = 0
longueur_chaine = len(chaine)
while compteur < longueur_chaine:
    print(chaine[compteur], end=' - * -')
    compteur+=1
print()

#Exemple - solution 2 : Parcourir une chaine de caractères et afficher le nom de voyelle et le nombre de consonnes
#Existence d'un opérateur <<in>> qui permet de tester la présence d'un élement dans un objet

chaine = input("Entrer la chaine à parcourir \n")
voyelles="aeiuoyAEIUOY"
nb_voyelles=0
nb_consonnes=0
compteur = 0
longueur_chaine = len(chaine)
while compteur < longueur_chaine:
    if chaine[compteur] in voyelles:
        nb_voyelles+=1
    else:
        nb_consonnes+=1
    compteur+=1
print(f"La chaine {chaine} comprend {nb_voyelles} voyelles et {nb_consonnes} consonnes")
print()

'''
#Exemple - solution 2 : Parcourir une chaine de caractères et afficher le nom de voyelle 
#et le nombre de consonnes. S'arrêter si on rencontre un espace
chaine = input("Entrer la chaine à parcourir \n")
voyelles="aeiuoyAEIUOY"
nb_voyelles=0
nb_consonnes=0
compteur = 0
longueur_chaine = len(chaine)
chaine1=""
while True:
    if compteur < longueur_chaine:
        if chaine[compteur] == " ":
            #Break arrête la boucle
            break
        elif chaine[compteur] in voyelles:
            nb_voyelles+=1
        else:
            nb_consonnes+=1
        chaine1+=chaine[compteur]
        compteur+=1
    else:
        break
print(f"La chaine {chaine1} comprend {nb_voyelles} voyelles et {nb_consonnes} consonnes")
print()

