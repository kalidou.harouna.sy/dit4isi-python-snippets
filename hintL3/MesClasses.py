#!/usr/bin/python3

class Personne():
    nom=""
    prenom=""
    def __init__(self, nom="", prenom=""):
        self.nom = nom
        self.prenom = prenom

class Etudiant(Personne):
    matricule=""

    def __init__(self, nom="", prenom="", matricule=""):
        super().__init__(nom,prenom)
        self.matricule = matricule
    def formattage(self):
        print("---------------------------")
        print(f"Matricule: {self.matricule}")
        print(f"Prenom: {self.prenom}")
        print(f"Nom: {self.nom}")
        print("---------------------------")


class Filiere():
    nom=""
    code=""
    classes=dict()

    def __str__(self):
        return self.nom+" - "+self.code

    def __init__(self,code,nom=""):
        if nom=="":
            self.nom=code
        else:
            self.nom = nom
        self.code=code

    def addClasse(self,maclasse):
        self.classes[maclasse.code] = maclasse
    
    def show(self):
        print("---------------------------------")
        print(f"Nom filière: {self.nom}")
        print(f"Code filière: {self.code}")
        if (len(self.classes) != 0):
            [print(f'----> {key} : {value.nom}') for key, value in self.classes.items() ]
        else:
            print("Cette filière n'a pas encore de classe")
        print("---------------------------------")
    
class Classe():
    nom=""
    code=""
    filiere=""
    etudiants=dict()

    def __init__(self, nom, code):
        self.nom = nom
        self.code = code
    
    def addFiliere(self,filiere):
        self.filiere = filiere
    
    def addStudent(self,monetudiant):
        if isintance(Etudiant):
            etudiants[monetudiant.matricule] = monetudiant
    
    def show(self):
        print("---------------------------------")
        print(f"Nom classe: {self.nom}")
        print(f"Code classe: {self.code}")
        print(f"filière : {self.filiere}")
        print("---------------------------------")