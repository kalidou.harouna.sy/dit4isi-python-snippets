def palindrome(chaine):
    return chaine == chaine[::-1]


def factoriel(n):
    resultat=1
    for i in range(1,n+1):
        #resultat=resultat*i
        resultat*=i
    return (resultat,n)


def matrice(n,m):
    A=[]
    for i in range(n):
        L=list()
        for j in range(m):
            try:
                a=int(input("Donner la valeur a{}{}\n".format(i+1,j+1)))
                L.append(a)
            except ValueError:
                print("Vous n'avez pas saisi un entier")
        A.append(L)
    return A

def formattageMatrice(A):
    for i in range(len(A)):
        for j in range(len(A[0])):
            print(A[i][j], end="\t")
        print()


def leftRotateMatrice(A):
    n=len(A)
    m=len(A[0])
    M=[]
    for j in range(m):
        L=[]
        for i in range(n,-1,-1):
            L.append(A[i][j])
        M.append(L)
    return M


def rightRotateMatrice(A):
    n=len(A)
    m=len(A[0])
    M=[]
    for j in range(m-1,-1,-1):
        L=[]
        for i in range(n):
            L.append(A[i][j])
        M.append(L)
    return M


def factorielRecursive(n):
    if n==0 or n==1:
        return 1
    else:
        return n*factorielRecursive(n-1)


def saisie(nature, message):
    a=input("%s\n"%(message))
    if nature =="entier":
        while not a.isdigit():
            print("Merci de saisir un entier pour continuer.")
            a=input("%s\n"%(message))
    elif nature =="chaine":
        while not a.isalpha():
            print("Merci de saisir une chaine de caractères alphabétique pour continuer.")
            a=input("%s\n"%(message))
    elif nature =="alphanumerique":
        while not a.isalnum():
            print("Merci de saisir une chaine de caractères alphanumérique pour continuer.")
            a=input("Merci de saisir une valeur de type %s\n"%nature)

    return a

def createClasse():
    nom=input("Indiquer le nom de la classe")
    filiere=input("Indiquer la filière")
    code=input("Indiquer le code de la classe")

    return (code, nom, filiere)