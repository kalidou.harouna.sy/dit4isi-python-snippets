from MesClasses import *
from fonctions import *
import json


ecole=dict()

def saveData(data):
    fichier = open('ecole.txt','w')
    json.dump(data,fichier)
    fichier.close()


def loadData():
    try:
        fichier = open('ecole.txt','r')
    except FileNotFoundError:
        print("Le fichier sera créé au prochain ajout d'un élément")
        data=dict()
    else:
        data=json.load(fichier)
        fichier.close()
    return data

def ajouterFiliere():
    nom_fil = input("Veuillez entrer le nom de la filière")
    code_fil = saisie("alphanumerique", "Veuillez entrer le code de la filière")
    return Filiere(code_fil, nom_fil)

def ajouterClasse():
    nom_classe = input("Veuillez entrer le nom de la classe")
    code_classe = saisie("alphanumerique", "Veuillez entrer le code de la classe")
    return Classe(nom_classe, code_classe)

while True:
    print("1. Taper 1 pour manager une filière")
    print("2. Taper 2 pour manager une classe")
    print("3. Taper 3 pour manager un etudiant")
    print("4. Taper tout autre touche pour quitter")
    choix = saisie("entier", "Veuillez effectuer un choix")
    data = loadData()
    if len(data)==0:
        ecole=dict()
    else:
        ecole=data
    if (int(choix) == 1):
        print("1. Taper 1 pour ajouter une filière")
        print("2. Taper 2 pour rattacher une classe à une filière")
        print("3. Taper 3 pour afficher les détails de la filière")
        choixfiliere = saisie("entier", "Veuillez effectuer un choix")
        if (int(choixfiliere) == 1):
            mafiliere = ajouterFiliere()
            print(mafiliere.__dict__)
            ecole[mafiliere.code] = json.dumps(mafiliere.__dict__)
            saveData(ecole)
            print(str(mafiliere))
        elif (int(choixfiliere) == 2):
            pass
        elif (int(choixfiliere) == 3):
            code_fil = saisie("alphanumerique", "Veuillez entrer le code de la filière")
            if (code_fil in ecole.keys()):
                mafiliere = ecole[code_fil]
                mafiliere.show()
            else:
                print("Le code de la filière saisie est incorrect")
        else:
            print("Votre choix n'est pas accepté")

    elif (int(choix) == 2):
        print("1. Taper 1 pour ajouter une classe")
        print("2. Taper 2 pour afficher les détails d'une")
        choix_classe = saisie("entier", "Veuillez effectuer un choix")
        if (int(choix_classe) == 1):
            maclasse = ajouterClasse()
            code_fil = saisie("alphanumerique", "Veuillez entrer le code de la filière")
            if (code_fil in ecole.keys()):
                maclasse.addFiliere(ecole[code_fil].nom)
                ecole[code_fil].addClasse(maclasse)
            else:
                print("Le code de la filière saisie est incorrect")
        elif (int(choixfiliere) == 2):
            pass
        elif (int(choixfiliere) == 3):
            code_fil = saisie("alphanumerique", "Veuillez entrer le code de la filière")
            if (code_fil in ecole.keys()):
                mafiliere = ecole[code_fil]
                code_classe = saisie("alphanumerique", "Veuillez entrer le code de la classe")
                if (code_classe in mafiliere.classes.keys()):
                    mafiliere.classes[code_classe].show()
            else:
                print("Le code de la filière saisie est incorrect")
        else:
            print("Votre choix n'est pas accepté")
    elif (int(choix) == 3):
        pass
    else:
        print("Goodbye!!!")
        break
